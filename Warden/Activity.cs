﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Warden
{
    public partial class Activity : Form
    {
        Dictionary<int, string> codes_activities = new Dictionary<int, string>();
        Dictionary<string, string> activities_tranclate = new Dictionary<string, string>();
        Dictionary<string, string> mapNames_tranclate = new Dictionary<string, string>();
        string Id = "";
        string name = "";
        JObject selectedActivity = null;
        List<string> characterId = new List<string>();
        DataGridView dg = null;
        public Activity(string id, string name, DataGridView dg)
        {
            InitializeComponent();
            Id = id;
            this.dg = dg;
            this.name = name;
            #region
            codes_activities.Add(0, "None");
            codes_activities.Add(2, "Story");
            codes_activities.Add(3, "Налёт");
            codes_activities.Add(4, "Рейд");
            codes_activities.Add(5, "PvP активность");
            codes_activities.Add(6, "Patrol");
            codes_activities.Add(7, "PvE активность");
            codes_activities.Add(9, "Reserved9");
            codes_activities.Add(10, "Control");
            codes_activities.Add(11, "Reserved11");
            codes_activities.Add(12, "Clash");
            codes_activities.Add(13, "Reserved13");
            codes_activities.Add(15, "CrimsonDoubles");
            codes_activities.Add(16, "Сумрачный налёт");
            codes_activities.Add(17, "HeroicNightfall");
            codes_activities.Add(18, "Налёты");
            codes_activities.Add(19, "IronBanner");
            codes_activities.Add(20, "Reserved20");
            codes_activities.Add(21, "Reserved21");
            codes_activities.Add(22, "Reserved22");
            codes_activities.Add(24, "Reserved24");
            codes_activities.Add(25, "AllMayhem");
            codes_activities.Add(26, "Reserved26");
            codes_activities.Add(27, "Reserved27");
            codes_activities.Add(28, "Reserved28");
            codes_activities.Add(29, "Reserved29");
            codes_activities.Add(30, "Reserved30");
            codes_activities.Add(31, "Supremacy");
            codes_activities.Add(32, "PrivateMatchesAll");
            codes_activities.Add(37, "Survival");
            codes_activities.Add(38, "Countdown");
            codes_activities.Add(39, "TrialsOfTheNine");
            codes_activities.Add(40, "Social");
            codes_activities.Add(41, "TrialsCountdown");
            codes_activities.Add(42, "TrialsSurvival");
            codes_activities.Add(43, "IronBannerControl");
            codes_activities.Add(44, "IronBannerClash");
            codes_activities.Add(45, "IronBannerSupremacy");
            codes_activities.Add(46, "ScoredNightfall");
            codes_activities.Add(47, "ScoredHeroicNightfall");
            codes_activities.Add(48, "Стычка");
            codes_activities.Add(49, "AllDoubles");
            codes_activities.Add(50, "Doubles");
            codes_activities.Add(51, "PrivateMatchesClash");
            codes_activities.Add(52, "PrivateMatchesControl");
            codes_activities.Add(53, "PrivateMatchesSupremacy");
            codes_activities.Add(54, "PrivateMatchesCountdown");
            codes_activities.Add(55, "PrivateMatchesSurvival");
            codes_activities.Add(56, "PrivateMatchesMayhem");
            codes_activities.Add(57, "PrivateMatchesRumble");
            codes_activities.Add(58, "HeroicAdventure");
            codes_activities.Add(59, "Showdown");
            codes_activities.Add(60, "Lockdown");
            codes_activities.Add(61, "Scorched");
            codes_activities.Add(62, "ScorchedTeam");
            codes_activities.Add(63, "Гамбит");
            codes_activities.Add(64, "AllPvECompetitive");
            codes_activities.Add(65, "Breakthrough");
            codes_activities.Add(66, "Активация кузни");
            codes_activities.Add(67, "Salvage");
            codes_activities.Add(68, "IronBannerSalvage");
            codes_activities.Add(69, "PvPCompetitive");
            codes_activities.Add(70, "PvPQuickplay");
            codes_activities.Add(71, "ClashQuickplay");
            codes_activities.Add(72, "ClashCompetitive");
            codes_activities.Add(73, "ControlQuickplay");
            codes_activities.Add(74, "ControlCompetitive");
            codes_activities.Add(75, "Гамбит Прайм");
            codes_activities.Add(76, "Reckoning");
            codes_activities.Add(77, "Паноптикум");
            #endregion
            #region
            activities_tranclate.Add("None", "Не определено");
            activities_tranclate.Add("Story", "История");
            activities_tranclate.Add("Strike", "Налёт");
            activities_tranclate.Add("Raid", "Рейд");
            activities_tranclate.Add("AllPvP", "Все PvP");
            activities_tranclate.Add("Patrol", "Исследование");
            activities_tranclate.Add("AllPvE", "Все PvE");
            activities_tranclate.Add("Reserved9", "Резерв9");
            activities_tranclate.Add("Control", "Control");//
            activities_tranclate.Add("Reserved11", "Резерв11");
            activities_tranclate.Add("Clash", "Clash");//
            activities_tranclate.Add("Reserved13", "Резерв13");
            activities_tranclate.Add("CrimsonDoubles", "CrimsonDoubles");//
            activities_tranclate.Add("Nightfall", "Сумрачный налёт");
            activities_tranclate.Add("HeroicNightfall", "Героический сумрачный налёт");
            activities_tranclate.Add("AllStrikes", "Все налёты");
            activities_tranclate.Add("IronBanner", "Железное знамя");
            activities_tranclate.Add("Reserved20", "Резерв20");
            activities_tranclate.Add("Reserved21", "Резерв21");
            activities_tranclate.Add("Reserved22", "Резерв22");
            activities_tranclate.Add("Reserved24", "Резерв24");
            activities_tranclate.Add("AllMayhem", "Весь урон?");
            activities_tranclate.Add("Reserved26", "Резерв26");
            activities_tranclate.Add("Reserved27", "Резерв27");
            activities_tranclate.Add("Reserved28", "Резерв28");
            activities_tranclate.Add("Reserved29", "Резерв29");
            activities_tranclate.Add("Reserved30", "Резерв30");
            activities_tranclate.Add("Supremacy", "PvP - Превосходство");
            activities_tranclate.Add("PrivateMatchesAll", "Приватный матч");
            activities_tranclate.Add("Survival", "Выживание");
            activities_tranclate.Add("Countdown", "Countdown");//
            activities_tranclate.Add("TrialsOfTheNine", "Испытания девяти");
            activities_tranclate.Add("Social", "Social");//
            activities_tranclate.Add("TrialsCountdown", "TrialsCountdown");//
            activities_tranclate.Add("TrialsSurvival", "TrialsSurvival");//
            activities_tranclate.Add("IronBannerControl", "Железное знамя - контроль");
            activities_tranclate.Add("IronBannerClash", "Железное знамя - столкновение");
            activities_tranclate.Add("IronBannerSupremacy", "Железное знамя - превосходство");
            activities_tranclate.Add("ScoredNightfall", "Сумрачный налёт - счёт");
            activities_tranclate.Add("ScoredHeroicNightfall", "Героический сумрачный налёт - счёт");
            activities_tranclate.Add("Rumble", "Rumble");//
            activities_tranclate.Add("AllDoubles", "AllDoubles");//
            activities_tranclate.Add("Doubles", "Doubles");//
            activities_tranclate.Add("PrivateMatchesClash", "Приватный матч - столкновение");
            activities_tranclate.Add("PrivateMatchesControl", "Приватный матч - контроль");
            activities_tranclate.Add("PrivateMatchesSupremacy", "Приватный матч - превосходство");
            activities_tranclate.Add("PrivateMatchesCountdown", "Приватный матч - Countdown");
            activities_tranclate.Add("PrivateMatchesSurvival", "Приватный матч - выживание");
            activities_tranclate.Add("PrivateMatchesMayhem", "Приватный матч - Mayhem");//
            activities_tranclate.Add("PrivateMatchesRumble", "Приватный матч - Rumble");//
            activities_tranclate.Add("HeroicAdventure", "Героическое приключение");
            activities_tranclate.Add("Showdown", "Showdown");//
            activities_tranclate.Add("Lockdown", "Lockdown");//
            activities_tranclate.Add("Scorched", "Scorched");//
            activities_tranclate.Add("ScorchedTeam", "ScorchedTeam");//
            activities_tranclate.Add("Gambit", "Гамбит");
            activities_tranclate.Add("AllPvECompetitive", "Все PvE активности");
            activities_tranclate.Add("Breakthrough", "Breakthrough");//
            activities_tranclate.Add("BlackArmoryRun", "Активация кузни");
            activities_tranclate.Add("Salvage", "Salvage");//
            activities_tranclate.Add("IronBannerSalvage", "Железное знамя - Salvage");//
            activities_tranclate.Add("PvPCompetitive", "PvP - Состязательная игра");
            activities_tranclate.Add("PvPQuickplay", "PvP - Быстрая игра");
            activities_tranclate.Add("ClashQuickplay", "PvP - Столкновение в быстрой игре");
            activities_tranclate.Add("ClashCompetitive", "PvP - Столкновение в состязательной игре");
            activities_tranclate.Add("ControlQuickplay", "PvP - Контроль в быстрой игре");
            activities_tranclate.Add("ControlCompetitive", "PvP - Контроль в состязательной игре");
            activities_tranclate.Add("GambitPrime", "Гамбит прайм");
            activities_tranclate.Add("Reckoning", "Паноптикум");
            #endregion
            mapNames_tranclate.Add("The Arms Dealer", "Торговец оружием");
            mapNames_tranclate.Add("Lake of Shadows", "Озеро теней");
            mapNames_tranclate.Add("Tree of Probabilities", "Древо вероятностей");
            mapNames_tranclate.Add("Strange Terrain", "Глубины Марса");
            mapNames_tranclate.Add("Savathûn's Song", "Песнь Саватун");
            mapNames_tranclate.Add("SavathÃ»n's Song", "Песнь Саватун");
            mapNames_tranclate.Add("The Pyramidion", "Пирамидион");
            mapNames_tranclate.Add("Exodus Crash", "Крушение 'Исхода'");
            mapNames_tranclate.Add("The Inverted Spire", "Вывернутый шпиль");
            mapNames_tranclate.Add("A Garden World", "Сад");
            mapNames_tranclate.Add("Will of the Thousands", "Повелитель тысяч армий");
            mapNames_tranclate.Add("Insight Terminus", "Терминал знаний");
            mapNames_tranclate.Add("The Hollowed Lair", "Полое логово");
            mapNames_tranclate.Add("Warden of Nothing", "Надзиратель пустоты");
            mapNames_tranclate.Add("The Corrupted", "Оскверненная");
            mapNames_tranclate.Add("Crown of Sorrow", "Корона скорби");
            mapNames_tranclate.Add("Hellas Basin", "Марс");
            mapNames_tranclate.Add("The Tangled Shore", "Спутанные берега");
            mapNames_tranclate.Add("European Dead Zone", "ЕМЗ");
            mapNames_tranclate.Add("Nessus, Unstable Centaur", "Несс");
            mapNames_tranclate.Add("The Dreaming City", "Город грёз");
            mapNames_tranclate.Add("Titan", "Титан");
            mapNames_tranclate.Add("Normal", "Обычный");
            mapNames_tranclate.Add("Heroic", "Героический");
            
        }
        private string get_http(string url, bool fromTracker = false)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("accept-encoding", "gzip, deflate");
            if (!fromTracker)
                request.AddHeader("Host", "www.bungie.net");
            else
                request.AddHeader("Host", "destinytracker.com");
            request.AddHeader("Postman-Token", "40b98e8f-2bc4-45ce-8805-547a30a6622c,bdfbddbe-296d-4a55-9134-8f3e71e62d2e");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
            if (!fromTracker)
                request.AddHeader("X-API-Key", "96df364ab7c044d3aac5e7d6daab4512");
            IRestResponse res = client.Execute(request);
            return res.Content;
        }

        #region preload
        private void Activity_Load(object sender, EventArgs e)
        {
            groupBox2.Text = name;
            getCharset();
            listView1.SmallImageList = imageList;
            Focus();
        }
        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            getCharset();
        }
        private void getCharset()
        {
            listBox3.Items.Clear();
            linkLabel2.Visible = !true;
            Application.DoEvents();
            int countError = -1;
            string res = "";
            do
            {
                countError++;
                if (countError > 10) break;
                res = get_http("https://www.bungie.net/Platform/Destiny2/3/Profile/" + Id + "?components=200");
            }
            while (res.ToStringNew() == "");

            JObject o = JObject.Parse(res);
            if (o.SelectToken("ErrorCode").ToStringNew() != "1") { linkLabel2.Visible = true; return; }


            foreach (var item in o.SelectToken("Response.characters.data"))
            {
                var id = item.First.SelectToken("characterId").ToStringNew();
                var clasid = item.First.SelectToken("classType").ToStringNew();
                string nameclass = "";
                switch (clasid)
                {
                    case "0": nameclass = "Титан"; break;
                    case "1": nameclass = "Охотник"; break;
                    case "2": nameclass = "Варлок"; break;
                    default:
                        break;
                }
                listBox3.Items.Add(nameclass + " - " + id);
            }
        }
        #endregion

        #region отрисовка
        List<int> listColor = new List<int>();
        List<int> listColorNames = new List<int>();
        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == -1) return;
            listColor.Sort();
            e.DrawBackground();
            Brush myBrush = Brushes.Black;
            Brush BlackBrush = Brushes.Black;
            FontStyle myStyle = FontStyle.Regular;
            //Button temp = sender as Button;
            //int index = listBox_ExNotesNames.IndexFromPoint(temp.Location);
            if (listColor.BinarySearch(e.Index) > -1)
            {
                myBrush = Brushes.Olive;
            }
            e.Graphics.DrawString(((ListBox)sender).Items[e.Index].ToString(), new Font(e.Font.Name, e.Font.Size, myStyle), myBrush, e.Bounds, StringFormat.GenericDefault);
        }
        #endregion

        /// <summary>
        /// Получение активностей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string count = textBox1.Text;
            List<string> listIdAkt = new List<string>();
            listBox1.Items.Clear();
            characterId.Clear();
            listColor.Clear();
            foreach (string item in listBox3.SelectedItems)
            {
                characterId.Add(item.Split('-')[1]);
            }
            Application.DoEvents();
            //пробег по всем чарам и получение id активностей
            foreach (var item in characterId)
            {
                JObject o = JObject.Parse(get_http("https://www.bungie.net/Platform/Destiny2/3/Account/" + Id + "/Character/" + item + "/Stats/Activities/?count=" + count));
                string error = (string)o.SelectToken("ErrorCode");

                if (error == "1665")
                    listBox1.Items.Add("Активность скрыта");
                else if (error != "1")
                    continue;

                try
                {
                    listIdAkt.AddRange(getAktForDate(o, textBox2.Text.ToInt()));
                }
                catch { }
            }

            if (!checkBox2.Checked)
            {
                foreach (var item in listIdAkt)
                {
                    string act = codes_activities[item.Split('-')[2].ToInt()];
                    try { act = activities_tranclate[act]; }
                    catch { }
                    listBox1.Items.Add(item.Split('-')[0] + " - " + act + " (" + item.Split('-')[1]+")");
                }
                return;
            }
            
            //пробег по всем активностям

            for (int id = 0; id < listIdAkt.Count; id++)
            {
                string act = codes_activities[listIdAkt[id].Split('-')[2].ToInt()];
                try { act = activities_tranclate[act]; }
                catch { }
                textBox1.Text = id + "/" + listIdAkt.Count;
                listBox1.Items.Add(listIdAkt[id].Split('-')[0] + " - " + act + " (" + listIdAkt[id].Split('-')[1]+")");
                
                Application.DoEvents();
                JObject o = JObject.Parse(get_http("https://www.bungie.net/Platform/Destiny2/Stats/PostGameCarnageReport/" + listIdAkt[id].Split('-')[1]));
                string error = (string)o.SelectToken("ErrorCode");
                if (error != "1")
                {
                    continue;
                }
                //пробег по всем игрокам текущей активности
                foreach (var item in o.SelectToken("Response.entries"))
                {
                    string playerId = item.SelectToken("player.destinyUserInfo.membershipId").ToStringNew();
                    string playerName = item.SelectToken("player.destinyUserInfo.displayName").ToStringNew();
                    //listBox2.Items.Add(playerName);
                    if (playerName == groupBox2.Text) continue;
                    for (int j = 0; j < dg.RowCount; j++)
                    {
                        if (playerId == dg.Rows[j].Cells["bungieMembershipId"].Value.ToStringNew())
                        {
                            listColor.Add(id);
                          //  listColorNames.Add(listBox2.Items.Count - 1);
                            break;
                        }
                    }
                }
            }
            textBox1.Text = count;
            
        }
        private List<string> getAktForDate(JObject akt, int period = -7)
        {
            DateTime to = DateTime.Now.AddDays(0 - period);
            List<string> res = new List<string>();
            int count = (checkBox1.Checked) ? 1 : 0;
            foreach (var item in akt.SelectToken("Response.activities"))
            {
                var temp = Convert.ToDateTime(item.SelectToken("period").ToStringNew()).AddHours(3);
                if (temp >= to && item.SelectToken("values.playerCount.basic.value").ToInt() > count)
                    res.Add(temp + "-" + item.SelectToken("activityDetails.instanceId").ToStringNew() + "-" + item.SelectToken("activityDetails.mode").ToStringNew());
            }

            return res;
        }

        ImageList imageList = new ImageList() { ImageSize = new Size(30,30) };
        /// <summary>
        /// Подробная статистика
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listColorNames.Clear();
            listView1.Items.Clear();
            if (!listBox1.SelectedItem.ToStringNew().Contains("(")) return;
            groupBox1.Text = listBox1.SelectedItem.ToStringNew();
            Application.DoEvents();

            //  JObject o = JObject.Parse(get_http("https://www.bungie.net/Platform/Destiny2/Stats/PostGameCarnageReport/" +
            //     groupBox1.Text.Split('(')[1].Replace(")","")));


            var res = get_http("https://destinytracker.com/d2/api/pgcr/" + listBox1.SelectedItem.ToStringNew().Split('(')[1].Replace(")", ""), true);
            if (res == "")
                return;
            JObject o = null;
                try
            {
                o = JObject.Parse(res);
            }
            catch { return; }

            
            selectedActivity = o;
            //Response
            labelTime.Text = o.SelectToken("period").ToStringNew();


            var nameActivity = codes_activities[o.SelectToken("activityDetails.mode").ToInt()];//
            try
            {
                nameActivity = activities_tranclate[nameActivity];
            }
            catch { }
            var activityIcon = "https://cdn.thetrackernetwork.com/destiny/" + o.SelectToken("activityDetails.readable.activityIcon").ToStringNew();//
            var activityTypeName = o.SelectToken("activityDetails.readable.activityTypeName").ToStringNew();
            var mapIcon = "https://cdn.thetrackernetwork.com/destiny/" + o.SelectToken("activityDetails.readable.mapIcon").ToStringNew();
            var mapNameTemp = o.SelectToken("activityDetails.readable.mapName").ToStringNew();//
            string mapName = "";

            var temp = mapNameTemp.Split(':');
            if (temp.Count() > 1)
            {
                try
                {
                    mapName = activities_tranclate[temp[0].Trim()];
                }
                catch
                {
                    try
                    {
                        mapName = mapNames_tranclate[temp[0].Trim()];
                    }
                    catch
                    {
                        mapName = temp[0].Trim();
                    }
                }
                try
                {
                    mapName += ": " + mapNames_tranclate[temp[1].Trim()];
                }
                catch { }
            }
            try
            {
                mapName = mapNames_tranclate[mapNameTemp];
            }
            catch { }
            if (mapName == "" || mapName == ":")
                mapName = mapNameTemp;
            mapName = mapName.Replace(nameActivity + ":", "");

            var modeDisplay = o.SelectToken("activityDetails.readable.modeDisplay").ToStringNew();//
            try
            {
                modeDisplay = activities_tranclate[modeDisplay];
            }
            catch
            { }
            var pgcrImage = "https://cdn.thetrackernetwork.com/destiny/" + o.SelectToken("activityDetails.readable.pgcrImage").ToStringNew();//

            pictureBox1.LoadAsync(activityIcon);
            labelNameActivity.Text = nameActivity;
            if (modeDisplay != nameActivity)
                labelSubNameActivity.Text = modeDisplay;
            else
                labelSubNameActivity.Text = "";
            labelMapName.Text = mapName;
            pictureBoxMap.LoadAsync(pgcrImage);

            //пробег по всем игрокам текущей активности
            foreach (var item in o.SelectToken("entries"))
            {
                string playerId = item.SelectToken("player.destinyUserInfo.membershipId").ToStringNew();
                string playerName = item.SelectToken("player.destinyUserInfo.displayName").ToStringNew();
                string playerIcon = "https://cdn.thetrackernetwork.com/destiny/" + item.SelectToken("player.destinyUserInfo.iconPath").ToStringNew();
                string playerClass = item.SelectToken("player.characterClass").ToStringNew();
                switch (playerClass)
                {
                    case "Warlock": playerClass = "Варлок"; break;
                    case "Titan": playerClass = "Титан"; break;
                    case "Hunter": playerClass = "Охотник"; break;
                    default:
                        break;
                }
                string playerKills = item.SelectToken("values.kills.basic.value").ToStringNew();
                string playerDeaths = item.SelectToken("values.deaths.basic.value").ToStringNew();
                string playerKD = item.SelectToken("values.killsDeathsRatio.basic.displayValue").ToStringNew();


                ListViewItem listViewItem = new ListViewItem(new string[] { "", (playerName == "") ? playerId : playerName, playerClass, playerKills, playerDeaths, playerKD, playerId });
                imageList.Images.Add(LoadImage(playerIcon));
                listViewItem.ImageIndex = imageList.Images.Count-1;
                
                

               
                if (playerName != groupBox2.Text && checkBox2.Checked) 
                {
                    for (int j = 0; j < dg.RowCount; j++)
                    {
                        if (playerId == dg.Rows[j].Cells["bungieMembershipId"].Value.ToStringNew())
                        {
                            listViewItem.ForeColor = Color.Olive;
                            break;
                        }
                    }
                }
                listView1.Items.Add(listViewItem);
                Application.DoEvents();
            }
            //listBox2.Focus();
            //listBox1.Focus();
        }
        private Image LoadImage(string url)
        {
            System.Net.WebRequest request =
                System.Net.WebRequest.Create(url);

            System.Net.WebResponse response = request.GetResponse();
            System.IO.Stream responseStream =
                response.GetResponseStream();

            Bitmap bmp = new Bitmap(responseStream);

            responseStream.Dispose();

            return bmp;
        }

        private void ListView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Activity activity = new Activity(listView1.SelectedItems[0].SubItems[listView1.SelectedItems[0].SubItems.Count-1].Text, listView1.SelectedItems[0].SubItems[1].Text, dg) ;
            activity.ShowDialog();
        }
    }
}

