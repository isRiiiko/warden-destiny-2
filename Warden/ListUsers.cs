﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Warden
{
    public partial class ListUsers : Form
    {
        List<string> ls = new List<string>();
        public ListUsers(List<string> l)
        {
            ls = l;
            InitializeComponent();
        }

        private void ListUsers_Load(object sender, EventArgs e)
        {
            foreach (var item in ls)
            {
                listBox1.Items.Add(item);
            }
        }
    }
}
