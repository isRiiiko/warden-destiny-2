﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Warden
{
    public partial class Compare : Form
    {
        public Compare()
        {
            InitializeComponent();
        }

        private void Compare_Load(object sender, EventArgs e)
        {
            var data = GetDataOnRequest("select * from Users where UserName = ''");
            for (int i = 2; i < data.Columns.Count; i++)
            {
                listBox1.Items.Insert(0, data.Columns[i].ColumnName);
                listBox2.Items.Insert(0, data.Columns[i].ColumnName);
            }
        }

        #region Работа с БД
        public OleDbConnection Connect()
        {
            string nowDate = DateTime.Now.ToShortDateString().Replace(".", "_");
            string ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + Application.StartupPath + "\\db.mdb";
            OleDbConnection conn = new OleDbConnection();
            conn.ConnectionString = ConnectionString;
            return conn;
        }
        public DataTable GetDataOnRequest(string SQLrequest)
        {
            var c = Connect();
            c.Open();
            DataTable data = new DataTable();
            OleDbCommand SQLQuery = new OleDbCommand(SQLrequest, c);
            OleDbDataAdapter dataAdapter = new OleDbDataAdapter(SQLQuery);
            try
            {
                dataAdapter.Fill(data);
            }
            catch (Exception q) { MessageBox.Show("Ошибка: " + q.Message + "\r\rЗапрос: " + SQLrequest, "Ошибка запроса.", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
            c.Dispose();
            return data;
        }
        public int SetDataOnRequest(string SQLrequest)
        {
            var c = Connect();
            c.Open();
            int count = 0;
            OleDbCommand SQLQuery = new OleDbCommand(SQLrequest, c);
            try
            {
                count = SQLQuery.ExecuteNonQuery();
            }
            catch (Exception q) { MessageBox.Show("Ошибка: " + q.Message + "\r\rЗапрос: " + SQLrequest, "Ошибка запроса.", MessageBoxButtons.OK, MessageBoxIcon.Warning); }

            c.Dispose();
            return count;
        }
        #endregion

        private void calcStat()
        {
            if (listBox1.SelectedItem == null || listBox2.SelectedItem == null)
            {
                label6.Text = label6.Tag.ToString() + "-";
                label7.Text = label7.Tag.ToString() + "-";
                return;
            }
            DateTime now1 = Convert.ToDateTime(listBox1.SelectedItem.ToStringNew().Split('-')[0].Trim().Replace("_", "."));
            DateTime now2 = Convert.ToDateTime(listBox2.SelectedItem.ToStringNew().Split('-')[0].Trim().Replace("_", "."));

            label6.Text = label6.Tag.ToString() + (Math.Abs((now1 - now2).TotalDays));

            int count = 0;
            int sum = 0;
            for (int j = 0; j < dataGridView1.RowCount; j++)
            {
                if (dataGridView1[5, j].Value.ToInt() > 0)
                {
                    count++;
                    sum += dataGridView1[5, j].Value.ToInt();

                }
                
            }
            int res = 0;
            if (count != 0)
                res = sum / count;

            var day = new TimeSpan(0, 0, res).Days;
            var hour = new TimeSpan(0, 0, res).Hours;
            var min = new TimeSpan(0, 0, res).Minutes;

            label7.Text = label7.Tag.ToString() + ((day != 0) ? day + "d " + hour + "h" : hour + "h " + min + "m");
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1) { dataGridView1.Rows.Clear(); return; }
            for (int w = 0; w < dataGridView1.RowCount; w++)
            {
                dataGridView1[1, w].Value = "-1";
                dataGridView1[2, w].Value = "";
                dataGridView1[5, w].Value = "-1";
                dataGridView1[6, w].Value = "";
            }
            string now = listBox1.SelectedItem.ToStringNew();
            var data = GetDataOnRequest("select UserName, [" + now + "] from Users");


            for (int i = 0; i < data.Rows.Count; i++)
            {
                if (data.Rows[i][1].ToInt() == 0) continue;
                bool exist = false;
                for (int j = 0; j < dataGridView1.RowCount; j++)
                {
                    if (dataGridView1[0, j].Value.ToStringNew() == data.Rows[i][0].ToStringNew())
                    {
                        exist = true;
                        dataGridView1[1, j].Value = data.Rows[i][1].ToInt();
                        int day = new TimeSpan(0, 0, dataGridView1[1, j].Value.ToInt()).Days;
                        int hour = new TimeSpan(0, 0, dataGridView1[1, j].Value.ToInt()).Hours;
                        int min = new TimeSpan(0, 0, dataGridView1[1, j].Value.ToInt()).Minutes;
                        dataGridView1[2, j].Value = (day != 0) ? day + "d " + hour + "h" : hour + "h " + min + "m";

                        if (dataGridView1[3, j].Value.ToStringNew() != "")
                        {
                            dataGridView1[5, j].Value = Math.Abs(dataGridView1[1, j].Value.ToInt() - dataGridView1[3, j].Value.ToInt());
                            day = new TimeSpan(0, 0, dataGridView1[5, j].Value.ToInt()).Days;
                            hour = new TimeSpan(0, 0, dataGridView1[5, j].Value.ToInt()).Hours;
                            min = new TimeSpan(0, 0, dataGridView1[5, j].Value.ToInt()).Minutes;
                            dataGridView1[6, j].Value = (day != 0) ? day + "d " + hour + "h" : hour + "h " + min + "m";
                        }
                        break;
                    }
                }
                if (!exist)
                {
                    int day = new TimeSpan(0, 0, data.Rows[i][1].ToInt()).Days;
                    int hour = new TimeSpan(0, 0, data.Rows[i][1].ToInt()).Hours;
                    int min = new TimeSpan(0, 0, data.Rows[i][1].ToInt()).Minutes;
                    string temp = (day != 0) ? day + "d " + hour + "h" : hour + "h " + min + "m";

                    dataGridView1.Rows.Add(data.Rows[i][0].ToStringNew(), data.Rows[i][1].ToInt(), temp, -1, "", -1, "");
                }
            }
            checkNewName();
            calcStat();
            
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex == -1) { dataGridView1.Rows.Clear(); return; }
            for (int w = 0; w < dataGridView1.RowCount; w++)
            {
                dataGridView1[4, w].Value = "";
                dataGridView1[3, w].Value = "-1";
                dataGridView1[5, w].Value = "-1";
                dataGridView1[6, w].Value = "";
            }
            string now = listBox2.SelectedItem.ToStringNew();
            var data = GetDataOnRequest("select UserName, [" + now + "] from Users");

            for (int i = 0; i < data.Rows.Count; i++)
            {
                if (data.Rows[i][1].ToInt() == 0) continue;
                bool exist = false;
                for (int j = 0; j < dataGridView1.RowCount; j++)
                {
                    if (dataGridView1[0, j].Value.ToStringNew() == data.Rows[i][0].ToStringNew())
                    {
                        exist = true;
                        dataGridView1[3, j].Value = data.Rows[i][1].ToInt();
                        int day = new TimeSpan(0, 0, dataGridView1[3, j].Value.ToInt()).Days;
                        int hour = new TimeSpan(0, 0, dataGridView1[3, j].Value.ToInt()).Hours;
                        int min = new TimeSpan(0, 0, dataGridView1[3, j].Value.ToInt()).Minutes;
                        dataGridView1[4, j].Value = (day != 0) ? day + "d " + hour + "h" : hour + "h " + min + "m";

                        if (dataGridView1[1, j].Value.ToStringNew() != "")
                        {
                            dataGridView1[5, j].Value = Math.Abs(dataGridView1[1, j].Value.ToInt() - dataGridView1[3, j].Value.ToInt());
                            day = new TimeSpan(0, 0, dataGridView1[5, j].Value.ToInt()).Days;
                            hour = new TimeSpan(0, 0, dataGridView1[5, j].Value.ToInt()).Hours;
                            min = new TimeSpan(0, 0, dataGridView1[5, j].Value.ToInt()).Minutes;
                            dataGridView1[6, j].Value = (day != 0) ? day + "d " + hour + "h" : hour + "h " + min + "m";
                        }
                        break;
                    }
                }
                if (!exist)
                {
                    int day = new TimeSpan(0, 0, data.Rows[i][1].ToInt()).Days;
                    int hour = new TimeSpan(0, 0, data.Rows[i][1].ToInt()).Hours;
                    int min = new TimeSpan(0, 0, data.Rows[i][1].ToInt()).Minutes;
                    string temp = (day != 0) ? day + "d " + hour + "h" : hour + "h " + min + "m";

                    dataGridView1.Rows.Add(data.Rows[i][0].ToStringNew(), -1, "", data.Rows[i][1].ToInt(), temp, -1, "");
                }
            }
            checkNewName();
            calcStat();
        }
        private void checkNewName() {
            for (int j = 0; j < dataGridView1.RowCount; j++)
            {
                if (dataGridView1[2, j].Value.ToStringNew() == "" || dataGridView1[4, j].Value.ToStringNew() == "")
                {
                    dataGridView1[6, j].Value = "";
                    dataGridView1[5, j].Value = -1;
                }
            }

            }
        private void dataGridView1_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.Column.Index == 2)
            {
                if (dataGridView1[1, e.RowIndex1].Value.ToInt(true) > dataGridView1[1, e.RowIndex2].Value.ToInt(true))
                    e.SortResult = 1;
                else if (dataGridView1[1, e.RowIndex1].Value.ToInt(true) < dataGridView1[1, e.RowIndex2].Value.ToInt(true))
                    e.SortResult = -1;
                else
                    e.SortResult = 0;

                e.Handled = true;
            }
            else if (e.Column.Index == 4)
            {
                if (dataGridView1[3, e.RowIndex1].Value.ToInt(true) > dataGridView1[3, e.RowIndex2].Value.ToInt(true))
                    e.SortResult = 1;
                else if (dataGridView1[3, e.RowIndex1].Value.ToInt(true) < dataGridView1[3, e.RowIndex2].Value.ToInt(true))
                    e.SortResult = -1;
                else
                    e.SortResult = 0;

                e.Handled = true;
            }
            else if (e.Column.Index == 6)
            {
                if (dataGridView1[5, e.RowIndex1].Value.ToInt(true) > dataGridView1[5, e.RowIndex2].Value.ToInt(true))
                    e.SortResult = 1;
                else if (dataGridView1[5, e.RowIndex1].Value.ToInt(true) < dataGridView1[5, e.RowIndex2].Value.ToInt(true))
                    e.SortResult = -1;
                else
                    e.SortResult = 0;

                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = -1;
            listBox2.SelectedIndex = -1;
            calcStat();
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            compare();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            compare();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            compare();
        }
        private void compare()
        {
            Color color = Color.LightSalmon;
            int d = textBox1.Text.ToInt();
            int h = textBox2.Text.ToInt();
            int m = textBox3.Text.ToInt();
            int s = new TimeSpan(d, h, m, 0, 0).TotalSeconds.ToInt();
            string text = (d != 0) ? d + "d " + h + "h" : h + "h " + m + "m";

            if (d == 0 && m == 0 && h == 0)
            {
                for (int i = 0; i < dataGridView1.RowCount; i++)
                    for (int j = 0; j < dataGridView1.ColumnCount; j++)
                        dataGridView1.Rows[i].DefaultCellStyle.BackColor = SystemColors.Window;
                return;
            }
            switch (comboBox1.SelectedIndex)
            {
                //>
                case 0:
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        if (dataGridView1[5,i].Value.ToInt() > s && dataGridView1[6, i].Value.ToStringNew() != "")
                            for (int j = 0; j < dataGridView1.ColumnCount; j++)
                                dataGridView1.Rows[i].DefaultCellStyle.BackColor = color;
                        else
                            for (int j = 0; j < dataGridView1.ColumnCount; j++)
                                dataGridView1.Rows[i].DefaultCellStyle.BackColor = SystemColors.Window;
                    }
                    break;
                //=
                case 1:
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        if (dataGridView1[6, i].Value.ToStringNew() == text)
                            for (int j = 0; j < dataGridView1.ColumnCount; j++)
                                dataGridView1.Rows[i].DefaultCellStyle.BackColor = color;
                        else
                            for (int j = 0; j < dataGridView1.ColumnCount; j++)
                                dataGridView1.Rows[i].DefaultCellStyle.BackColor = SystemColors.Window;
                    }
                    break;
                //<
                case 2:
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        if (dataGridView1[5, i].Value.ToInt() < s && dataGridView1[6, i].Value.ToStringNew() != "")
                            for (int j = 0; j < dataGridView1.ColumnCount; j++)
                                dataGridView1.Rows[i].DefaultCellStyle.BackColor = color;
                        else
                            for (int j = 0; j < dataGridView1.ColumnCount; j++)
                                dataGridView1.Rows[i].DefaultCellStyle.BackColor = SystemColors.Window;
                    }
                    break;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            compare();
        }
    }
}
