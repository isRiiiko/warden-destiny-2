﻿using System;
using System.Data;
using RestSharp;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections.Generic;

namespace Warden
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        void SetDoubleBuffered(Control c, bool value)
        {
            PropertyInfo pi = typeof(Control).GetProperty("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic);
            if (pi != null)
            {
                pi.SetValue(c, value, null);
            }

            MethodInfo mi = typeof(Control).GetMethod("SetStyle", BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.NonPublic);
            if (mi != null)
            {
                mi.Invoke(c, new object[] { ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true });
            }

            mi = typeof(Control).GetMethod("UpdateStyles", BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.NonPublic);
            if (mi != null)
            {
                mi.Invoke(c, null);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           /* var data1 = GetDataOnRequest("select UserName from Users");
            for (int i = 0; i < data1.Rows.Count; i++)
            {
               dataGridView1.Rows.Add("","",i+1, data1.Rows[i][0]);
            }*/
            SetDoubleBuffered(dataGridView1, true);
           // q();

            
           // return;
            var data = GetDataOnRequest("select * from Users where UserName = ''");
            for (int i = 2; i < data.Columns.Count; i++)
            {
                listBox1.Items.Insert(0, data.Columns[i].ColumnName);
            }
        }
        /// <summary>
        /// Информация о клане
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Загружаю информацию по клану ";
            Application.DoEvents();
            dataGridView1.Rows.Clear();
            nameLabel.Text = timeClanLabel.Text = "";
            avatarPictureBox.Image = bannerPictureBox.Image = null;
            nameClanLabel.Text =
            createdLabel.Text =
            membersLabel.Text = discLabel.Text = creatorLabel.Text = "";
            button2.Enabled = button7.Enabled = button5.Enabled = false;
            newUserLabel.Visible = delUserLabel.Visible = false;

            JObject o = JObject.Parse(get_http("https://www.bungie.net/Platform/GroupV2/" + IDClan));
            string error = (string)o.SelectToken("ErrorCode");
            if (error != "1")
            {
                nameClanLabel.Text = "Клан не существует, или API Bungie недоступен.";
                return;
            }
            nameClanLabel.Text = regReplace((string)o.SelectToken("Response.detail.name"));
            createdLabel.Text = createdLabel.Tag + Convert.ToDateTime(o.SelectToken("Response.detail.creationDate")).ToString("dd.MM.yyyy");
            membersLabel.Text = membersLabel.Tag + (string)o.SelectToken("Response.detail.memberCount");
            discLabel.Text = (string)o.SelectToken("Response.detail.motto");

            //creatorLabel.Text = creatorLabel.Tag + (string)o.SelectToken("Response.founder.destinyUserInfo.displayName");
            {
                try
                {
                    JObject o1 = JObject.Parse(get_http("https://www.bungie.net/Platform/User/GetMembershipsById/" +
                        (string)o.SelectToken("Response.founder.bungieNetUserInfo.membershipId") + "/3/"));
                    string error1 = (string)o1.SelectToken("ErrorCode");
                    if (error1 == "1")
                    {
                        string temp = (string)o1.SelectToken("Response.bungieNetUser.blizzardDisplayName");

                        if (temp.ToStringNew() == "")
                            temp = (string)o1.SelectToken("Response.destinyMemberships.displayName");
                        creatorLabel.Text = creatorLabel.Tag + temp;
                    }
                }
                catch { }

            }
            creatorLabel.Text += ((string)o.SelectToken("Response.founder.isOnline") == "False") ? " (offline)" : " (online)";

            {
                try
                {
                    JObject o1 = JObject.Parse(get_http("https://www.bungie.net/Platform/User/GetMembershipsById/"+
                        (string)o.SelectToken("Response.detail.membershipIdCreated") + "/3/"));
                    string error1 = (string)o1.SelectToken("ErrorCode");
                    if (error1 == "1")
                    {
                        string temp = (string)o1.SelectToken("Response.bungieNetUser.blizzardDisplayName");

                        if (temp.ToStringNew() == "")
                            temp = (string)o1.SelectToken("Response.destinyMemberships.displayName");
                        realCreatorLabel.Text = realCreatorLabel.Tag + temp;
                    }
                }
                catch { }
                
            }
            bannerPictureBox.Load("https://www.bungie.net" + (string)o.SelectToken("Response.detail.bannerPath"));
            logoPictureBox.Load("https://www.bungie.net" + (string)o.SelectToken("Response.detail.avatarPath"));
            bannerPictureBox.BackgroundImage = bannerPictureBox.Image;
            bannerPictureBox.Image = Properties.Resources.white;


            lockChange = true;
            dataGridView1.ReadOnly = true;
            dataGridView1.Rows.Clear();
            
            toolStripStatusLabel1.Text = "Загружаю список участников ";
            toolStripStatusLabel3.Text = "";
            Application.DoEvents();
            o = JObject.Parse(get_http("https://www.bungie.net/Platform/GroupV2/" + IDClan + "/Members/"));
            for (int i = 0; true; i++)
            {
                string name = (string)o.SelectToken("Response.results[" + i + "].destinyUserInfo.displayName");
                if (name == null) break;
                var join = Convert.ToDateTime(o.SelectToken("Response.results[" + i + "].joinDate"));
                var last = getUTC(Convert.ToInt32(o.SelectToken("Response.results[" + i + "].lastOnlineStatusChange")));
                string avatar = (string)o.SelectToken("Response.results[" + i + "].bungieNetUserInfo.iconPath");
                string MembershipId = (string)o.SelectToken("Response.results[" + i + "].destinyUserInfo.membershipId");
                var deltaClan = (DateTime.Now - join).Days;
                dataGridView1.Rows.Add(MembershipId, avatar, i + 1, name, join, deltaClan, last);

                Application.DoEvents();
            }
            lockChange = false;

            toolStripStatusLabel1.Text = "Готов ";
            button2.Enabled = button5.Enabled = button7.Enabled = true;
        }

        public string regReplace(string q)
        {
            Regex regex = new Regex(@"&#\d{4};");
            MatchCollection matches = regex.Matches(q);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    char r = (char)match.Value.Replace("&#", "").Replace(";", "").ToInt();
                    q = q.Replace(match.Value, r.ToString());
                }
            }
            return q;
        }
        /// <summary>
        /// Загрузка онлайна
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            lockChange = true;
            button2.Enabled = button5.Enabled = button1.Enabled = listBox1.Enabled = button7.Enabled = false;
            toolStripStatusLabel1.Text = "Загружаю статистику участников ";
            toolStripProgressBar1.Visible = true;
            toolStripProgressBar1.Maximum = dataGridView1.RowCount;
            Application.DoEvents();
            for (int j = 0; j < dataGridView1.RowCount; j++)
            {
                ThreadPool.QueueUserWorkItem(getStat, j);
            }

            lockChange = false;
        }
        /// <summary>
        /// Загрузка онлайна последоватенльно
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            lockChange = true;
            button2.Enabled = button5.Enabled = button1.Enabled = listBox1.Enabled = button7.Enabled = false;
            toolStripStatusLabel1.Text = "Загружаю статистику участников ";
            toolStripProgressBar1.Visible = true;
            toolStripProgressBar1.Maximum = dataGridView1.RowCount;
            Application.DoEvents();
            for (int j = 0; j < dataGridView1.RowCount; j++)
            {
                getStat(j);
            }
            button2.Enabled = button5.Enabled = button1.Enabled = listBox1.Enabled = button7.Enabled = true;
            lockChange = false;
        }
        #region Статистика
        private void getStat(object u)
        {
            int j = u.ToInt();
            string id = dataGridView1.Rows[j].Cells[0].Value.ToStringNew();
            if (id == "") return;

            int countError = -1;
            string res = "";
            do
            {
                countError++;
                if (countError > 10) break;
                res = get_http("https://www.bungie.net/Platform/Destiny2/3/Account/" + id + "/Stats/");
            }
            while (res.ToStringNew() == "");
            
            if (res.ToStringNew() == "")
            {
                Action action2 = () =>
                {
                    dataGridView1.Rows[j].Cells["totalSecondsColumn"].Value = 0;
                    dataGridView1.Rows[j].Cells["totalColumn"].Value = "Error";

                };
                this.InvokeCheck(action2);
            }
            else
            {
                JObject o = JObject.Parse(res);
                dataGridView1.Rows[j].Cells["characterId"].Value = "";
                Action action = () =>
                {
                    dataGridView1.Rows[j].Cells["totalSecondsColumn"].Value = o.SelectToken("Response.mergedAllCharacters.merged.allTime.totalActivityDurationSeconds.basic.value").ToInt();
                    dataGridView1.Rows[j].Cells["totalColumn"].Value = (string)o.SelectToken("Response.mergedAllCharacters.merged.allTime.totalActivityDurationSeconds.basic.displayValue");
                    foreach (var item in o.SelectToken("Response.characters"))
                    {
                        if (item.SelectToken("deleted").ToStringNew() != "True")
                            dataGridView1.Rows[j].Cells["characterId"].Value += item.SelectToken("characterId") + ",";
                    }
                };
                this.InvokeCheck(action);
            }
            Action end = () =>
            {
                try { toolStripProgressBar1.Value++; }
                catch { }
                toolStripStatusLabel3.Text = toolStripProgressBar1.Value + "/" + toolStripProgressBar1.Maximum;

                if (toolStripProgressBar1.Value == toolStripProgressBar1.Maximum)
                {
                    toolStripStatusLabel1.Text = "Готов ";
                    toolStripStatusLabel3.Text = "";
                    toolStripProgressBar1.Visible = false;
                    button2.Enabled = button5.Enabled = button1.Enabled = listBox1.Enabled  = button7.Enabled = true;
                    lockChange = false;
                    dataGridView1.ReadOnly = false;
                    toolStripProgressBar1.Value = 0;
                    listBox1.Enabled = true;
                }
            };
            this.InvokeCheck(end);
            Application.DoEvents();

        }

        private DateTime getUTC(int time)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(time).ToLocalTime();
            return dtDateTime/*.ToString("dd.MM.yyyy, HH:mm")*/;
        }

        private string get_http(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("accept-encoding", "gzip, deflate");
            request.AddHeader("Host", "www.bungie.net");
            request.AddHeader("Postman-Token", "40b98e8f-2bc4-45ce-8805-547a30a6622c,bdfbddbe-296d-4a55-9134-8f3e71e62d2e");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
            request.AddHeader("X-API-Key", "96df364ab7c044d3aac5e7d6daab4512");
            IRestResponse res = client.Execute(request);
            return res.Content;
        }
        #endregion

        bool lockChange = false;
        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (lockChange) return;
            nameLabel.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToStringNew();
            timeClanLabel.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToStringNew();
            inClanLabel.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToStringNew() + "д.";
            if (dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToStringNew() != "")
                avatarPictureBox.LoadAsync("https://www.bungie.net" + dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToStringNew());
            else
                avatarPictureBox.Image = null;
        }

    
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LinkLabel ll = (LinkLabel)sender;
            dataGridView1.Columns[ll.Tag.ToStringNew()].Visible = !dataGridView1.Columns[ll.Tag.ToStringNew()].Visible;
        }


        #region Работа с БД
        public OleDbConnection Connect()
        {
            string nowDate = DateTime.Now.ToShortDateString().Replace(".", "_");
            string ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + Application.StartupPath + "\\db.mdb";
            OleDbConnection conn = new OleDbConnection();
            conn.ConnectionString = ConnectionString;
            return conn;
        }
        public DataTable GetDataOnRequest(string SQLrequest)
        {
            var c = Connect();
            c.Open();
            DataTable data = new DataTable();
            OleDbCommand SQLQuery = new OleDbCommand(SQLrequest, c);
            OleDbDataAdapter dataAdapter = new OleDbDataAdapter(SQLQuery);
            try
            {
                dataAdapter.Fill(data);
            }
            catch (Exception q) { MessageBox.Show("Ошибка: " +q.Message + "\r\rЗапрос: " + SQLrequest, "Ошибка запроса.", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
            c.Dispose();
            return data;
        }
        public int SetDataOnRequest(string SQLrequest)
        {
            var c = Connect();
            c.Open();
            int count = 0;
            OleDbCommand SQLQuery = new OleDbCommand(SQLrequest, c);
            try
            {
                count = SQLQuery.ExecuteNonQuery();
            }
            catch (Exception q) { MessageBox.Show("Ошибка: " + q.Message + "\r\rЗапрос: " + SQLrequest, "Ошибка запроса.", MessageBoxButtons.OK, MessageBoxIcon.Warning); }

            c.Dispose();
            return count;
        }

        /// <summary>
        /// Записать
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Сохраняю текущий онлайн ";
            Application.DoEvents();
            string nowDate = DateTime.Now.ToShortDateString().Replace(".", "_");

            try
            {
                SetDataOnRequest("ALTER TABLE Users ADD [" + nowDate + " - " + IDClan + "] MEMO");
                listBox1.Items.Insert(0, nowDate + " - " + IDClan);
            }
            catch { }
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (SetDataOnRequest("UPDATE Users SET [" + nowDate + " - " + IDClan + "] = '" + dataGridView1[7, i].Value.ToStringNew() + "' WHERE UserName = '" + dataGridView1[3, i].Value.ToStringNew() + "'") == 0)
                    SetDataOnRequest("INSERT INTO Users (UserName, [" + nowDate + " - " + IDClan + "]) VALUES ('" + dataGridView1[3, i].Value.ToStringNew() + "', '" + dataGridView1[7, i].Value.ToStringNew() + "')");
            }

            toolStripStatusLabel1.Text = "Готов ";
        }

        /// <summary>
        /// Удаление
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            string now = listBox1.SelectedItem.ToStringNew();
            //alter table 'table_name' drop column 'column_name'
            try
            {
                SetDataOnRequest("ALTER TABLE Users drop column [" + now + "]");
                listBox1.Items.Remove(listBox1.SelectedItem);
            }
            catch { }
            
        }

        List<string> delUsers = new List<string>();
        /// <summary>
        /// Отобразить выбранный онлайн
        /// </summary>
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1) return;
            delUsers.Clear();

            toolStripStatusLabel1.Text = "Загружаю информацию по онлайну ";
            button1.Enabled = button2.Enabled = button4.Enabled = false;
            Application.DoEvents();


            for (int w = 0; w < dataGridView1.RowCount; w++)
            {
                dataGridView1[9, w].Value = "";
                dataGridView1[10, w].Value = "";
                dataGridView1[11, w].Value = "";
                dataGridView1[12, w].Value = "";
            }
            newUserLabel.Text = "";
            delUserLabel.Text = "";
            newUserLabel.Visible = false;
            delUserLabel.Visible = false;
            //string nowDate = DateTime.Now.ToShortDateString().Replace(".", "_");
            string now = listBox1.SelectedItem.ToStringNew();
            var data = GetDataOnRequest("select UserName, [" + now + "] from Users");
            bool exists = false;
            for (int i = 0; i < data.Rows.Count; i++)
            {
                exists = false;
                for (int j = 0; j < dataGridView1.RowCount; j++)
                {
                    if (dataGridView1[3, j].Value.ToStringNew() == data.Rows[i][0].ToStringNew())
                    {
                        
                        if (data.Rows[i][1].ToInt() == 0) break;
                        exists = true;
                        dataGridView1[9, j].Value = data.Rows[i][1].ToInt();
                        int day = new TimeSpan(0, 0, dataGridView1[9, j].Value.ToInt()).Days;
                        int hour = new TimeSpan(0, 0, dataGridView1[9, j].Value.ToInt()).Hours;
                        int min = new TimeSpan(0, 0, dataGridView1[9, j].Value.ToInt()).Minutes;
                        dataGridView1[10, j].Value = (day != 0) ? day + "d " + hour + "h" : hour + "h " + min + "m";


                        dataGridView1[11, j].Value = dataGridView1[7, j].Value.ToInt() - dataGridView1[9, j].Value.ToInt();
                        day = new TimeSpan(0, 0, dataGridView1[11, j].Value.ToInt()).Days;
                        hour = new TimeSpan(0, 0, dataGridView1[11, j].Value.ToInt()).Hours;
                        min = new TimeSpan(0, 0, dataGridView1[11, j].Value.ToInt()).Minutes;
                        dataGridView1[12, j].Value = (day != 0) ? day + "d " + hour + "h" : hour + "h " + min + "m";
                        
                        break;
                    }
                }
                if (!exists)
                {
                    delUsers.Add(data.Rows[i][0].ToStringNew());
                    delUserLabel.Text = delUserLabel.Tag.ToStringNew() + (delUserLabel.Text.Replace(delUserLabel.Tag.ToStringNew(), "").ToInt() + 1);
                    if (delUserLabel.Text != delUserLabel.Tag.ToStringNew())
                        delUserLabel.Visible = true;
                }
            }
            for (int j = 0; j < dataGridView1.RowCount; j++)
            {
                if (dataGridView1[9, j].Value.ToInt() == 0)
                    newUserLabel.Text = newUserLabel.Tag.ToStringNew() + (newUserLabel.Text.Replace(newUserLabel.Tag.ToStringNew(), "").ToInt() + 1);
            }
            if (newUserLabel.Text != newUserLabel.Tag.ToStringNew())
                newUserLabel.Visible = true;

            toolStripStatusLabel1.Text = "Готов ";
            button4.Enabled = true;
            if (dataGridView1.RowCount != 0)
                button1.Enabled = button2.Enabled = true;

        }

        #endregion


        private void dataGridView1_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.Column.Index == 8)
            {
                if (dataGridView1[7, e.RowIndex1].Value.ToInt() > dataGridView1[7, e.RowIndex2].Value.ToInt())
                    e.SortResult = 1;
                else if (dataGridView1[7, e.RowIndex1].Value.ToInt() < dataGridView1[7, e.RowIndex2].Value.ToInt())
                    e.SortResult = -1;
                else
                    e.SortResult = 0;

                e.Handled = true;
            }
            else if (e.Column.Index == 12)
            {
                if (dataGridView1[11, e.RowIndex1].Value.ToInt() > dataGridView1[11, e.RowIndex2].Value.ToInt())
                    e.SortResult = 1;
                else if (dataGridView1[11, e.RowIndex1].Value.ToInt() < dataGridView1[11, e.RowIndex2].Value.ToInt())
                    e.SortResult = -1;
                else
                    e.SortResult = 0;

                e.Handled = true;
            }
            else if (e.Column.Index == 10)
            {
                if (dataGridView1[9, e.RowIndex1].Value.ToInt() > dataGridView1[9, e.RowIndex2].Value.ToInt())
                    e.SortResult = 1;
                else if (dataGridView1[9, e.RowIndex1].Value.ToInt() < dataGridView1[9, e.RowIndex2].Value.ToInt())
                    e.SortResult = -1;
                else
                    e.SortResult = 0;

                e.Handled = true;
            }
            else if (e.Column.Index == 14)
            {
                int zn = 0;
                if (ModifierKeys == Keys.Control)
                {
                    MessageBox.Show("Сравнение будет осуществяляться по количеству активностей");
                    zn = 1;
                }
                
                var temp1 = (e.CellValue1.ToStringNew()== "") ? 0 : (e.CellValue1.ToStringNew() == "Доступ закрыт") ? -1 : e.CellValue1.ToStringNew().Split('/')[zn].ToInt();
                var temp2 = (e.CellValue2.ToStringNew()== "") ? 0 : (e.CellValue2.ToStringNew() == "Доступ закрыт") ? -1 : e.CellValue2.ToStringNew().Split('/')[zn].ToInt();
                if (temp1 > temp2)
                    e.SortResult = 1;
                else if (temp1 < temp2)
                    e.SortResult = -1;
                else
                    e.SortResult = 0;

                e.Handled = true;
            }

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (dataGridView1.Rows.Count == 0) return;
            if (searchTextBox.Text == "") return;
            if (e.KeyCode == Keys.Enter)
            {
                
                int start = 0;
                if (dataGridView1.CurrentRow != null)
                    start = dataGridView1.CurrentRow.Index+1;
                bool flag = false;
                for (int i = start; i < dataGridView1.RowCount; i++)
                {
                    for (int j = 0; j < dataGridView1.ColumnCount; j++)
                    {
                        if (!dataGridView1.Columns[j].Visible) continue;
                        if (dataGridView1[j, i].Value.ToStringNew().ToLower().Contains(searchTextBox.Text.ToLower()))
                        {
                            dataGridView1.CurrentCell = dataGridView1[j, i];
                            flag = true;
                            break;
                        }
                    }
                    if (flag) break;
                }

                if (!flag)
                {
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        for (int j = 0; j < dataGridView1.ColumnCount; j++)
                        {
                            if (!dataGridView1.Columns[j].Visible) continue;
                            if (dataGridView1[j, i].Value.ToStringNew().ToLower().Contains(searchTextBox.Text))
                            {
                                dataGridView1.CurrentCell = dataGridView1[j, i];
                                flag = true;
                                break;
                            }
                        }
                        if (flag) break;
                    }
                }
                if (flag) {
                    e.SuppressKeyPress = true;
                    e.Handled = true;
                }
            }
            
        }

        private void delUserLabel_Click(object sender, EventArgs e)
        {
            ListUsers ls = new ListUsers(delUsers);
            ls.Show();
        }


        private void button6_Click(object sender, EventArgs e)
        {
            Compare compare = new Compare();
            compare.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns["activity"].Visible = true;

            if (dataGridView1.Rows[0].Cells["activity"].Value.ToStringNew() != "")
            {
                var t = MessageBox.Show("Сохранить активности в файл?", "", MessageBoxButtons.YesNoCancel);
                if (t == DialogResult.Yes)
                {
                    SaveFileDialog saveFileDialog = new SaveFileDialog() { RestoreDirectory = true };
                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        string text = "";
                        for (int i = 0; i < dataGridView1.RowCount; i++)
                        {
                            text += dataGridView1.Rows[i].Cells["NameColumn"].Value.ToStringNew() + " - " + dataGridView1.Rows[i].Cells["activity"].Value.ToStringNew() + Environment.NewLine;
                        }
                        System.IO.File.WriteAllText(saveFileDialog.FileName, text);
                    }

                }
                else if (t == DialogResult.Cancel)
                    return;
            }

            var y = MessageBox.Show("Обновить активности из интернета?\n\tДа - обновить\n\tНет - загрузить из файла", "", MessageBoxButtons.YesNoCancel);
            if (y == DialogResult.No)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog() { Multiselect = false, RestoreDirectory = true };
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    var u = System.IO.File.ReadAllLines(openFileDialog.FileName);
                    foreach (var item in u)
                    {
                        for (int i = 0; i < dataGridView1.RowCount; i++)
                        {
                            if (dataGridView1.Rows[i].Cells["NameColumn"].Value.ToStringNew() == item.Split('-')[0].Trim())
                                dataGridView1.Rows[i].Cells["activity"].Value = item.Split('-')[1].Trim();
                        }
                    }
                }
                return;
            }
            else if (y == DialogResult.Cancel)
                return;

            if (dataGridView1.Rows[0].Cells["characterId"].Value.ToStringNew() == "")
            {
                
                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    if (dataGridView1.Rows[i].Cells["characterId"].Value.ToStringNew() != "") continue;
                    if (dataGridView1.Rows[i].Cells["activity"].Value.ToStringNew() != "") continue;

                    toolStripStatusLabel1.Text = "Загружаю персонажей игрока " + dataGridView1.Rows[i].Cells["NameColumn"].Value + " (" + (i + 1) + "/" + dataGridView1.RowCount + ")";
                    Application.DoEvents();
                    int countError = -1;
                    string res = "";
                    string error = "";
                    JObject o = null;
                    do
                    {
                        countError++;
                        if (countError > 10) break;
                        if (countError > 0)
                        {
                            toolStripStatusLabel1.Text = "Загружаю персонажей игрока " + dataGridView1.Rows[i].Cells["NameColumn"].Value + " (" + (i + 1) + "/" + dataGridView1.RowCount + ") Попытка " + (countError+1);
                            Application.DoEvents();
                        }
                        res = get_http("https://www.bungie.net/Platform/Destiny2/3/Profile/" + dataGridView1.Rows[i].Cells["bungieMembershipId"].Value + "?components=100");
                        o = JObject.Parse(res);
                        error = (string)o.SelectToken("ErrorCode");
                    }

                    while (error != "1");

                    if (error != "1") {
                        toolStripStatusLabel1.Text = "Не удалось загрузить персонажей " + dataGridView1.Rows[i].Cells["NameColumn"].Value + ", повторите попытку.";
                        return;
                    }

                    foreach (var item in o.SelectToken("Response.profile.data.characterIds"))
                    {
                        dataGridView1.Rows[i].Cells["characterId"].Value += item.ToStringNew() + ",";
                    }
                    
                }
            }
            getAkt(dataGridView1);

        }

        private void getAkt(object d)
        {
            DataGridView dg = (DataGridView)d;

            for (int i = 0; i < dg.RowCount; i++) // основной пробег по датагриду
            {
                if (dg.Rows[i].Cells["activity"].Value.ToStringNew() != "") continue;

                Action action2 = () =>
                {
                    toolStripStatusLabel1.Text = "Проверяю активности участника № " + (i+1);
                    Application.DoEvents();
                };
                this.InvokeCheck(action2);
               
                int count = 0;
                List<string> listIdAkt = new List<string>();
                //пробег по всем чарам и получение id активностей
                foreach (var item in dg.Rows[i].Cells["characterId"].Value.ToStringNew().Split(new char[] { ','}, StringSplitOptions.RemoveEmptyEntries))
                {
                    action2 = () =>
                    {
                        toolStripStatusLabel1.Text = "Проверяю активности участника № " + (i + 1) + " (Получаю активность персонажей)";
                        Application.DoEvents();
                    };
                    this.InvokeCheck(action2);
                    JObject o = JObject.Parse(get_http("https://www.bungie.net/Platform/Destiny2/3/Account/" + dg.Rows[i].Cells["bungieMembershipId"].Value + "/Character/" + item.Trim() + "/Stats/Activities/?count=70"));
                    string error = (string)o.SelectToken("ErrorCode");
                    if (error == "1665")
                        count = -1;
                    if (error != "1")
                    {
                        continue;
                    }
                    try
                    {
                        listIdAkt.AddRange(getAktForDate(o));
                    }
                    catch { }
                }
                //пробег по всем активностям
                
                for (int id = 0; id < listIdAkt.Count; id++)
                {
                    action2 = () =>
                    {
                        toolStripStatusLabel1.Text = "Проверяю активности участника № " + (i + 1) + " (Загружаю подробную статистику активности " + id + "/" + listIdAkt.Count + ")";
                        Application.DoEvents();
                    };
                    this.InvokeCheck(action2);
                    JObject o = JObject.Parse(get_http("https://www.bungie.net/Platform/Destiny2/Stats/PostGameCarnageReport/"+ listIdAkt[id]));
                    string error = (string)o.SelectToken("ErrorCode");
                    if (error != "1")
                    {
                        continue;
                    }
                    //пробег по всем игрокам текущей активности
                    foreach (var item in o.SelectToken("Response.entries"))
                    {
                        bool f = false;
                        string playerId = item.SelectToken("player.destinyUserInfo.membershipId").ToStringNew();
                        string playerName = item.SelectToken("player.destinyUserInfo.displayName").ToStringNew();
                        action2 = () =>
                        {
                            toolStripStatusLabel1.Text = "Проверяю активности участника № " + (i + 1) + " (Сверяю игрока "+ playerName + " из активности с текущим списком)";
                            Application.DoEvents();
                        };
                        this.InvokeCheck(action2);
                        for (int j = 0; j < dg.RowCount; j++)
                        {
                            if (j == i) continue;
                            if (playerId == dg.Rows[j].Cells["bungieMembershipId"].Value.ToStringNew())
                            {
                                count++;
                                f = true;
                                break;
                            }
                        }
                        if (f) break;
                    }
                }
                action2 = () =>
                {
                    dataGridView1.Rows[i].Cells["activity"].Value = (count != -1) ? count + "/" + listIdAkt.Count : "Доступ закрыт";
                    toolStripStatusLabel1.Text = "Готов";
                };
                this.InvokeCheck(action2);
            }
        }
        private List<string> getAktForDate(JObject akt, int period = -7) {
            DateTime to = DateTime.Now.AddDays(period);
            List<string> res = new List<string>();

            foreach (var item in akt.SelectToken("Response.activities"))
            {
                var temp = Convert.ToDateTime(item.SelectToken("period").ToStringNew());
                if (temp >= to && item.SelectToken("values.playerCount.basic.value").ToInt() > 1)
                    res.Add(item.SelectToken("activityDetails.instanceId").ToStringNew());
            }

            return res;
        }

        private void dataGridView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            if (e.Y <= dataGridView1.ColumnHeadersHeight) return;
            if (dataGridView1.CurrentCell == null) return;

            Activity activity = new Activity(dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells["bungieMembershipId"].Value.ToStringNew(), dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells["NameColumn"].Value.ToStringNew(), dataGridView1);
            activity.ShowDialog();
        }

        string IDClan = "3558686";
        private void IDtextBox_TextChanged(object sender, EventArgs e)
        {
            if (IDtextBox.Text != "")
                IDClan = IDtextBox.Text;
            else if (listBox2.SelectedIndex != -1)
                IDClan = listBox2.SelectedItem.ToStringNew();
            else
                IDClan = "3558686";
        }

        private void ListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IDtextBox.Text != "")
                IDClan = IDtextBox.Text;
            else if (listBox2.SelectedIndex != -1)
                IDClan = listBox2.SelectedItem.ToStringNew();
            else
                IDClan = "3558686";
        }
    }
    /// <summary>
    /// Добавление необходимых методов для любых типов.
    /// </summary>
    static class Expansion
    {
        /// <summary>
        /// Пытается привести объект к int32, если это невозможно, вернёт 0.
        /// </summary>
        /// <param name="ob">Объект</param>
        /// <returns></returns>
        public static int ToInt(this object ob)
        {
            int res = 0;
            try { res = Convert.ToInt32(ob); }
            catch { }
            return res;
        }
        /// <summary>
        /// Пытается привести объект к int32, если это невозможно, вернёт -1.
        /// </summary>
        /// <param name="ob">Объект</param>
        /// <returns></returns>
        public static int ToInt(this object ob, bool GetMeMinus)
        {
            int res = -1;
            try { res = Convert.ToInt32(ob); }
            catch { }
            return res;
        }
        /// <summary>
        /// Пытается привести объект к string, если это невозможно, вернёт пустую строку.
        /// </summary>
        /// <param name="ob"></param>
        /// <returns></returns>
        public static string ToStringNew(this object ob)
        {
            string value = "";
            try { value = Convert.ToString(ob); }
            catch { }
            return value;
        }
        /// <summary>
        /// Проверяет, необходимо ли использовать метод Invoke, и использует его, если нужно.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="action">Action action = () => {Код};</param>
        public static void InvokeCheck(this Control control, Action action)
        {
            try
            {
                if (control.InvokeRequired)
                    control.Invoke(action);
                else
                    action();
            }
            catch
            { }
        }
    }
    }
